from io import StringIO

import ruamel.yaml


def get_yaml():
    "Return a properly configured YAML object"
    yaml = ruamel.yaml.YAML(typ="rt")
    yaml.preserve_quotes = True
    return yaml


def strip_document_end_marker(s):
    "Useful as a transform when expecting single scalars"
    if s.endswith('...\n'):
        return s[:-4]
    return s


def dump_to_string(yaml, obj, **kwargs):
    """
    YAML values support a lot more different things than plain JSON
    so using YAML for serialisation is good for roundtripping.

    Wrapper, since ruamel doesn't have a function to dump to string.
    """
    stringio = StringIO()
    yaml.dump(obj, stringio, transform=strip_document_end_marker, **kwargs)
    return stringio.getvalue()
